#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Fixes torrent filename mismatches due to migrating between filesystems which have incompatible
# implementations of UTF. For example, HFS+ filenames cannot be converted (as far as we know) via a
# tool like convmv.
#
# Cobbled together by Gregor Robinson <gregor@fiatflux.is>.
# Torrent parsing code by Fredrik Lundh athttp://effbot.org/zone/bencode.py
#
# Released under GPLv3.
# Use at your own risk!

import logging
import re

logger = logging.getLogger('TorrentFilenameFixer')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
logger.addHandler(ch)

def tokenize(text, match=re.compile("([idel])|(\d+):|(-?\d+)").match):
    i = 0
    while i < len(text):
        m = match(text, i)
        s = m.group(m.lastindex)
        i = m.end()
        if m.lastindex == 2:
            yield "s"
            yield text[i:i+int(s)]
            i = i + int(s)
        else:
            yield s

def decode_item(next, token):
    if token == "i":
        # integer: "i" value "e"
        data = int(next())
        if next() != "e":
            raise ValueError
    elif token == "s":
        # string: "s" value (virtual tokens)
        data = next()
    elif token == "l" or token == "d":
        # container: "l" (or "d") values "e"
        data = []
        tok = next()
        while tok != "e":
            data.append(decode_item(next, tok))
            tok = next()
        if token == "d":
            data = dict(zip(data[0::2], data[1::2]))
    else:
        raise ValueError
    return data

def decode(text):
    try:
        src = tokenize(text)
        data = decode_item(src.next, src.next())
        for token in src: # look for more tokens
            raise SyntaxError("trailing junk")
    except (AttributeError, ValueError, StopIteration):
        raise SyntaxError("syntax error")
    return data

import glob
import hashlib
import os

class MappedFile:
    def __init__(self, codebook, disfigured_path_root, dest_dir,
            torrent, filenum, correct_relpath):
        self.disfigured_path_root = disfigured_path_root
        self.dest_dir = dest_dir
        self.torrent = torrent
        self.filenum = filenum

        self.correct_relpath = correct_relpath
        self.correct_qualified_path = os.path.abspath(os.path.join(dest_dir, correct_relpath))

        self.disfigured_index_path = os.path.abspath(os.path.join(codebook, torrent, filenum))
        self.disfigured_path = ''
        self.__get_disfigured_filename()
        assert self.disfigured_path, ("Can't find one of the files in the codebook: %s" %
                self.disfigured_index_path)
        self.disfigured_qualified_path = os.path.join(disfigured_path_root, self.disfigured_path)

    def __get_disfigured_filename(self):
        os.path.walk(self.disfigured_index_path, self.__disfigured_path_visitor, None)
        return self.disfigured_path

    def get_correct_filename(self):
        return self.correct_qualified_path

    def get_disfigured_filename(self):
        return self.disfigured_qualified_path

    def get_correct_rel_filename(self):
        return self.correct_relpath

    def get_disfigured_rel_filename(self):
        return self.disfigured_path

    def __disfigured_path_visitor(self, arg, dirname, names):
        if len(names) != 1:
            logger.error('invalid number of names in codebook path... %s\n %s' % (dirname, names))
            return
        assert len(names) == 1
        assert not self.disfigured_path
        qualified_name = os.path.abspath(
                os.path.join(dirname, names[0]))
        if not os.path.isfile(qualified_name):
            return
        self.disfigured_path = os.path.relpath(qualified_name, self.disfigured_index_path)

class TorrentFilenameFixer:
    def __init__(self, torrent_dir, disfigured_path_root):
        self.torrent_dir = os.path.abspath(torrent_dir)
        self.disfigured_path_root = os.path.abspath(disfigured_path_root)

    def write_tree(self):
        tree = self.__generate_tree()
        for path in self.__generate_tree():
            directory = os.path.dirname(path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            with open(path, 'a'):
                os.utime(path, None)

    def rename_paths(self, codebook, dest_dir):
        master_tree = self.__generate_tree()
        num_disfigured_files = 0
        num_ok_files = 0
        num_missing_files = 0
        for master_branch in master_tree:
            disfigured_branch = os.path.relpath(master_branch, self.disfigured_path_root)
            torrent, filenum, correct_relpath = \
                    disfigured_branch.strip(os.path.sep).split(os.path.sep, 2)
            try:
                mf = MappedFile(codebook, self.disfigured_path_root,
                        dest_dir, torrent, filenum, correct_relpath)
            except Exception, e:
                logger.error(e)
                continue

            c_abs = mf.get_correct_filename()
            d_abs = mf.get_disfigured_filename()
            if os.path.exists(d_abs):
                c = mf.get_correct_rel_filename()
                d = mf.get_disfigured_rel_filename()

                if not c == d:
                    num_disfigured_files += 1
                else:
                    num_ok_files += 1

                dir = os.path.dirname(mf.get_correct_filename())
                if not os.path.exists(dir):
                    os.makedirs(dir)
                os.rename(d_abs, c_abs)

            else:
                num_missing_files += 1
                continue

        logger.info(("Fixed %d files, found %d files with accurate filenames, " +
                "and failed to locate %d others.") % (num_disfigured_files, num_ok_files, num_missing_files))

    def __generate_tree(self):
        torrentfile_names = self.__list_torrents()
        for fname in torrentfile_names:
            torrent_data = decode(open(fname).read())
            info = torrent_data['info']

            torrent_name = info['name']

            m = hashlib.sha1()
            m.update(''.join(info['pieces']))
            fingerprint = m.hexdigest()

            try:
                filenames = ['/'.join(os.path.join(f)) for f in self.__get_filenames(torrent_data)]
            except:
                logger.warning('Unable to find file list for torrent file %s' % fname)

            for filenumber, filename in enumerate(filenames):
                yield os.path.join(self.disfigured_path_root, fingerprint, repr(filenumber), torrent_name, filename)

    def __list_torrents(self):
        return glob.glob('/'.join((self.torrent_dir, '*.torrent')))

    def __get_filenames(self, torrent):
        return [file['path'] for file in torrent['info']['files']]

def main():
    import argparse
    parser = argparse.ArgumentParser(prog='torrent_filename_fixer')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--generate', nargs=2,
            metavar=('torrent_dir', 'codebook_dest'))
    group.add_argument('--decode', nargs=4,
            metavar=('torrent_dir', 'download_dir', 'codebook', 'dest_dir'))
    args = parser.parse_args()

    if args.generate:
        torrent_dir, codebook_dest = args.generate
        tff = TorrentFilenameFixer(torrent_dir, codebook_dest)
        tff.write_tree()
    elif args.decode:
        torrent_dir, download_dir, codebook, dest_dir = args.decode
        download_dir = os.path.abspath(download_dir)
        tff = TorrentFilenameFixer(torrent_dir, download_dir)
        tff.rename_paths(codebook, dest_dir)

if __name__ == '__main__':
    main()
