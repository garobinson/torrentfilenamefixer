#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Look, this is extremely platform-dependent. But I don't really use other platforms so I'm not the
# best person to help you out. The coverage isn't that great either, but this test is enough to
# convince the original author that the script works well enough to point toward one copy of a
# torrent library. If you want this test to be a better safeguard, or for it to work on non-POSIX
# machines, I will be happy to accept pull requests. 

import libtorrent as lt
import os
import shutil
import tempfile
import unittest

from torrent_filename_fixer import *

class TestTorrentFilenameFixer(unittest.TestCase):
    def setUp(self):
        self.tempdir = tempfile.mkdtemp(prefix='tfilenamefixertest_')

        self.torrent_dir = os.path.join(self.tempdir, 'torrents')
        os.makedirs(self.torrent_dir)
        self.codebook_dir = os.path.join(self.tempdir, 'codebook')
        os.makedirs(self.codebook_dir)
        self.download_dir = os.path.join(self.tempdir, 'download')
        os.makedirs(self.download_dir)
        self.dest_dir = os.path.join(self.tempdir, 'dest_dir')
        os.makedirs(self.dest_dir)

        torrent1_dir = 'The Outer Directory - með ógeðslegum táknum! (2015) [FLAC]'
        torrent1_dir_abs = os.path.join(self.download_dir, torrent1_dir)
        os.makedirs(torrent1_dir_abs)
        # Add some fake files to the download directory.
        with open(os.path.join(torrent1_dir_abs, 'example_0.flac'), 'wb') as f:
            f.write('random junk')
        with open(os.path.join(torrent1_dir_abs, 'example_1.flac'), 'wb') as f:
            f.write('more random junk')
        torrent1_inner_dir = 'inner dir'
        torrent1_inner_dir_abs = os.path.join(torrent1_dir_abs, torrent1_inner_dir)
        os.makedirs(torrent1_inner_dir_abs)
        with open(os.path.join(torrent1_inner_dir_abs, 'með ógeðslegum táknum!.cue'), 'wb') as f:
            f.write('even more random junk')

        torrent2_dir = '柏大輔 - program music 1 (2007) [FLAC]'
        torrent2_dir_abs = os.path.join(self.download_dir, torrent2_dir)
        os.makedirs(torrent2_dir_abs)
        # Add some fake files to the download directory.
        with open(os.path.join(torrent2_dir_abs, '01 Stella.flac'), 'wb') as f:
            f.write('random junk~')
        with open(os.path.join(torrent2_dir_abs, '02 Write Once, Run Melos.flac'), 'wb') as f:
            f.write('more random junk!')
        torrent2_inner_dir = '柏大輔'
        torrent2_inner_dir_abs = os.path.join(torrent2_dir_abs, torrent2_inner_dir)
        os.makedirs(torrent2_inner_dir_abs)
        self.torrent1_innerfilename = os.path.join(torrent2_dir, torrent2_inner_dir, 'badass_cover.jpg')
        torrent1_innerfilename_abs = os.path.join(self.download_dir, self.torrent1_innerfilename)
        with open(os.path.join(self.download_dir, self.torrent1_innerfilename), 'wb') as f:
            f.write('even more random junk?')

        self.torrent_fingerprints = {}
        self.populate_torrent_dir()
        self.tff = TorrentFilenameFixer(self.torrent_dir, self.codebook_dir)

    def populate_torrent_dir(self):
        # For each directory in download_dir, create a torrent file.
        for i, d in enumerate(sorted(next(os.walk(self.download_dir))[1])):
            wdir = os.path.join(self.download_dir, d)
            fs = lt.file_storage()
            lt.add_files(fs, wdir)
            t = lt.create_torrent(fs)
            t.add_tracker('what.tracker/would/you/use?k=vh28yu34fnrs')
            lt.set_piece_hashes(t, self.download_dir)

            torrent_file = os.path.join(self.torrent_dir, d+'.torrent')

            t_data = t.generate()
            m = hashlib.sha1()
            m.update(''.join(t_data['info']['pieces']))
            self.torrent_fingerprints[i] = m.hexdigest()

            with open(torrent_file, 'wb') as f:
                f.write(lt.bencode(t_data))

    def test_write_tree(self):
        self.tff.write_tree()
        expected_codebook_path = os.path.join(
                self.codebook_dir,
                self.torrent_fingerprints[1],
                repr(0),
                self.torrent1_innerfilename)
        self.assertTrue(os.path.exists(expected_codebook_path))

    def test_rename_paths(self):
        self.test_write_tree()
        self.tff = TorrentFilenameFixer(self.torrent_dir, self.download_dir)
        original_codebook_path = os.path.join(
                self.codebook_dir,
                self.torrent_fingerprints[1],
                repr(0),
                self.torrent1_innerfilename)
        original_download_path = os.path.join(
                self.download_dir,
                self.torrent1_innerfilename)
        restored_path = os.path.join(
                self.dest_dir,
                self.torrent1_innerfilename)
        self.assertTrue(os.path.exists(original_codebook_path))
        junked_download_path = os.path.join(
                self.download_dir,
                ''.join((self.torrent1_innerfilename, '__junked')))
        junked_codebook_path = os.path.join(
                self.codebook_dir,
                self.torrent_fingerprints[1],
                repr(0),
                ''.join((self.torrent1_innerfilename, '__junked')))
        os.rename(original_download_path, junked_download_path)
        os.rename(original_codebook_path, junked_codebook_path)

        self.tff.rename_paths(self.codebook_dir, self.dest_dir)
        self.assertTrue(os.path.exists(restored_path))

    def tearDown(self):
        shutil.rmtree(self.tempdir)

if __name__ == '__main__':
    unittest.main()
