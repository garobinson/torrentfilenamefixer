#Torrent Filename Fixer
## Automatically rename files that naughty filesystems screwed up
In the event that you download your torrents onto a crappy filesystem like HFS+ which mangles UTF filenames, it can be desirable to rename your downloads upon switching to a filesystem with full UTF support. This tool can accomplish this automatically.

__Please only use if you understand the code, as it's mostly just designed for my annoying one-off situation. (Moral of the story: don't use HFS+.)__